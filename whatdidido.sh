#!/bin/bash
# by pbrisbin 2009
# modified by wm 2020-11-17
###

message() {
  echo -e "Usage:\n  ${0##*/} M"
  echo "     Show packages upgraded in the last M"
  echo "     full system updates (default 1)"
  echo
  echo "  ${0##*/} M N"
  echo "     Show what i've upgraded looking back"
  echo "     between minimum N and maximum M -Syu's"
  exit 1
}

# pacman log
log='/var/log/pacman.log'

# some temp files
tmp='/tmp/pac_timeline'
tmp2='/tmp/pac_upgrades'

# a help
[ "$1" = '--help' ] && message

# $0 M maybe N
case $# in
  0) M=1; N=0;;
  1) M=$1; ((M<1)) && M=1; N=0;;
  2) M=$1; ((M<1)) && M=1; N=$2;
     ((M==N)) && ((N--)); ((N>M)) && { i=$M; M=$N; N=$i; unset i;};;
  *) message;;
esac

grep '.*upgraded.*(.*)$\|starting full system upgrade' $log | \
	cut -d ']' -f 3 | uniq > $tmp

max=$(grep -c '^ starting' $tmp) # number of all -Syu upgrades

count=1
while read line; do
  echo "$count $line" >> $tmp2
  ((count++))
done < <(grep -n '^ starting' $tmp)

line_M=$(awk '/^'$((max-M+1))' /{print $2}' $tmp2 | cut -d ":" -f 1)

if [ $N -eq 0 ]; then
  line_N=$(wc -l < $tmp)
else
  line_N=$(awk '/^'$((max-N+1))' /{print $2}' $tmp2 | cut -d ":" -f 1)
  ((line_N--))
fi
echo "-------------[  -Syu $M  ]---------------<"
head -n$((line_N)) $tmp | tail -n$((line_N-line_M))| \
	sed 's/starting full system upgrade/----------------------------</'

if [ $N -eq 0 ]; then
  echo "-------------[    Now    ]---------------<"
else
  echo "-------------[  -Syu $N  ]---------------<"
fi

#echo M=$M N=$N
#echo count=$count
#echo line_N=$line_N
#echo line_M=$line_M
#echo line_N-line_M-1=$((line_N-line_M))
rm $tmp $tmp2
