#!/bin/bash
sync && {
	diff /boot/vmlinuz-linux{,.efi} >/dev/null 2>&1 || {
		cp /boot/vmlinuz-linux{,.efi};
		echo vmlinuz-linux.efi has been updated;
	}
}

# see /usr/share/libalpm/hooks/90-mkinitcpio-install.hook
