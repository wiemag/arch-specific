arch-specific
=============

Shell scripts specific to Arch Linux, which basically means relating to the pacman package manager.

needireboot 	- If kernel was updated it is reccomended to reboot.<br />
whatdidido.sh 	- Lists previous system updates.<br />
99-vmlinuz_efi.hook    - update vmlinuz.efi if vmlinuz is updated<br />
99-vmlinuz_efi.hook.sh - update vmlinuz.efi if vmlinuz is updated<br />
